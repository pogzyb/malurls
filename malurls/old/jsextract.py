# JavaScript Extract and Parser
import re
import base64
import requests
from malurls.lib.parsers.baseparser import BaseParser
from malurls.lib.utilities import is_ad_server
from malurls.lib.logger import LogSomething


class JsMunger(BaseParser):

    def __init__(self, conf=None):
        super().__init__(conf)

    # put it all together
    def run(self, html_scripts, src_scripts, parsing=False):
        # parsing extracted data
        for i_s_pair in src_scripts:
            i, link = i_s_pair[0], i_s_pair[1]
            # base64 encoding ids and links to avoid mongodb errors
            i = base64.b64encode(i.encode()).decode('utf-8')
            if not is_ad_server(link):
                src_script = self.load_src_script(link)
                link = base64.b64encode(link.encode()).decode('utf-8')
                if parsing:
                    self.handle_script(i, src_script)
                else:
                    self.add_to_data_alone(keys=(i, link), item=src_script)
                    # self.data[i][link] = src_script
            else:
                self.add_to_data_alone(keys=("ad_server_links", i), item=link)
        # handle scripts on page
        for ind, page_script in enumerate(html_scripts):
            key = "script_on_page"
            if parsing:
                self.handle_script(key, page_script)
            else:
                self.add_to_data_alone(keys=(key, str(ind)), item=page_script)
        return self.data

    # load script from src link
    @staticmethod
    def load_src_script(link):
        try:
            resp = requests.get(link, timeout=2)
            return resp.text
        except ConnectionError as e:
            LogSomething.log(f"Error: {e} loading src_script at {link}")
            pass

    # function to handle script parsing
    def handle_script(self, i, script):
        for f in self.conf["keyFuncs"]:
            self.find_func(i, f, script)
        self.find_documents(i, script)
        self.find_variables(i, script)
        return

    # find standard functions
    def find_func(self, i, func, script):
        pattern = func
        found = re.findall()
        pass

    # find document.x
    def find_documents(self, i, script):
        pattern = "document\.\w+"
        found = re.findall(pattern, script, re.DOTALL)
        self.add_to_data_list(keys=(i, "document"), item=found)

    # find variables
    def find_variables(self, i, script):
        pattern = "var([\w\s]+)="
        found = re.findall(pattern, script, re.DOTALL)
        self.add_to_data_list(keys=(i, "var"), item=found)
