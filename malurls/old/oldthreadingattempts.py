# from threading import Thread
# from queue import Queue
# from concurrent.futures import ThreadPoolExecutor


# # UNSUCCESSFUL POOLING EXAMPLE
# def run_with_workers(filename, status, limit):
#     # open file
#     with open(filename, 'r') as file:
#         urls = ["http://" + line.rstrip("\n") for line in file if len(line) > 5]
#         if limit:
#             urls = urls[:limit]
#         with ThreadPoolExecutor(4) as executor:
#             executor.map(run, urls)
#
#
# # run it all
# def run(url):
#     this_process = BrowserProcess.start_browser_process("./browser.sh")
#     try:
#         collector(url, this_process.port, 1)
#     except Exception as e:
#         LogSomething.log(f"[ ERROR ] {e}; Trying collect {url};")
#     finally:
#         this_process.kill()


# class CollectionWorker(Thread):
#
#     def __init__(self, queue):
#         Thread.__init__(self)
#         self.queue = queue
#
#     def run(self):
#         while True:
#             # Get the work from the queue and expand the tuple
#             url, status = self.queue.get()
#             this_process = BrowserProcess.start_browser_process("./browser.sh")
#             time.sleep(randrange(2, 5))
#             print("[ INFO ] Pausing for safety...")
#             try:
#                 collector(url, this_process.port, status)
#             except Exception as e:
#                 LogSomething.log(f"[ ERROR ] {e}; Trying collect {url};")
#             finally:
#                 this_process.kill()
#
#
# def run_with_workers(filename, statuses, limit):
#     queue = Queue()
#     with open(filename, 'r') as file:
#         urls = ["http://" + line.lstrip(".").rstrip("\n") for line in file if len(line) > 5]
#     if limit:
#         urls = urls[:limit]
#     for x in range(4):
#         worker = CollectionWorker(queue)
#         # Setting daemon to True will let the main thread exit even though the workers are blocking
#         worker.daemon = True
#         worker.start()
#     # add to queue
#     for url in urls:
#         LogSomething.log(f"[ MESSAGE QUEUE ] {url} has been added to the queue!")
#         queue.put((url, statuses))
#     queue.join()
#     LogSomething.log(f"[ COMPLETED MESSAGE QUEUE ]")