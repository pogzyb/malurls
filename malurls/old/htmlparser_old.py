# HTML Parser

"""
This is before I knew about html.parser in the Python Standard Library.

Not deleting because some functionality may be recycled.

Renaming.

- Joe
09/19/2018

"""

import re
from malurls.lib.utilities import avg_length, max_length


class HTMLParse(object):

    def __init__(self, dynamic_html, data_conf):
        self.html = dynamic_html
        self.data_conf = data_conf
        self.data = dict()
        self.scripts_on_page = list()

    # runs and returns data
    def run(self):
        self.data["html-length"] = len(self.html)
        for tag in self.data_conf["html"]["keyElements"]:
            # retrieve tag elements
            found = self.element_tag_parser(tag)
            # add elements to data dict
            self.add_to_data(keys=("tags", tag), item=found)
            # compute stats on elements
            self.tag_stats(found, tag)
        # add scripts_on_page to data dict
        self.tag_stats(self.scripts_on_page, )

    # Add to "data" hash;
    # Useful to avoid repeating try-excepts everywhere;
    # Assumes keys has length 2
    def add_to_data(self, keys, item):
        k1, k2 = keys
        try:
            self.data[k1][k2].append(item)
        except KeyError:
            try:
                self.data[k1][k2] = [item]
            except KeyError:
                self.data[k1] = {
                    k2: [item]
                }

    # adds feature data into data dict
    def tag_stats(self, tags_list, tag):
        stats = [
            ("average-length", avg_length),
            ("max-length", max_length),
            ("count", len)
        ]
        for key, func in stats:
            # self.add_to_data(keys=(key, tag), item=func(tags_list))
            # previous implementation
            if key in self.data.keys():
                self.data[key][tag] = func(tags_list)
            else:
                self.data[key] = {
                    tag: func(tags_list)
                }

    # return a list of tag elements w/o children (ex: <div class="main" onload="" .. >)
    def element_tag_parser(self, tag):
        pattern = "<" + tag + "[^>]+>"
        if tag == "script":
            found = self.filter_page_scripts()
        else:
            found = re.findall(pattern, self.html.replace(" ", ""), re.IGNORECASE)
        return found

    # remove full scripts from page; append to another list
    def filter_page_scripts(self):
        pattern = "<script[^>]+>.*?</script>"
        src_scripts = list()
        scripts = re.findall(pattern, self.html.replace(" ", "").lower(), re.DOTALL)
        for i, elem in enumerate(scripts):
            if "src=" not in elem.lower():
                self.scripts_on_page.append(elem)
            else:
                src_scripts.append(elem)
        # only return script tags with a "src="
        return src_scripts



