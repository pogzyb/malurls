# test cases
import unittest
import json
from malurls.lib.parsers.html_parse import HtmlMunger


class TestHTMLParse(unittest.TestCase):

    def setUp(self):
        print("Setting up...")
        html = open('./files_tests/html_static.txt', 'r')
        self.raw_html = html.read()
        html.close()
        print("...setUp complete!")

    def test_parser_run(self):
        munger = HtmlMunger.from_html(self.raw_html)
        print("Testing parser...\nwriting data to file...")
        with open("./files_tests/html_parser_output.json", "w") as out:
            out.write(json.dumps(munger.data, indent=4, sort_keys=True, default=str))
        print("...parser testing complete!\n")

        munger.extract_from_data()
        # print(munger.munged_data)
        with open("./files_tests/html_munged_output.json", "w") as out:
            out.write(json.dumps(munger.munged_data, indent=4, sort_keys=True, default=str))
        print("...munged data testing complete!\n")

    def test_extract_data(self):
        pass


if __name__ == "__main__":
    unittest.main()
