import unittest
import json
from malurls.lib.parsers.cookies_parse import CookieJar


class TestCookies(unittest.TestCase):

    def setUp(self):
        print("[ INFO ] Setting Up...")
        test_data = open('./files_tests/debug_data_outputgithub.com.json', 'r')
        # data is a list of "cookie" dictionaries
        self.data = json.loads(test_data.read())["data"]["browser"]["cookies"]
        test_data.close()
        print("[ INFO ] Set up complete!  self.data is available.")

    def test_sanity_CookieJar_clsmethod(self):
        print("[ STARTING ] test_CookieJar_classmethod")
        cookies = CookieJar.from_data(self.data)
        print(cookies.data)
        print("[ PASSED ] test_CookieJar_classmethod")