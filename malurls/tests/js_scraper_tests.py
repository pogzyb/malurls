# test cases
import unittest
import json
from malurls.lib.parsers.js_parse import JsMunger


class TestJsParser(unittest.TestCase):

    def setUp(self):
        try:
            open_src_scripts = open("./files_tests/src_scripts.json", "r")
            open_raw_scripts = open("./files_tests/on_page_scripts.json", "r")
        except OSError:
            print("Couldn't open files...")
        else:
            with open_src_scripts as oss:
                self.src_scripts = json.loads(oss.read())
            with open_raw_scripts as ors:
                self.html_scripts = json.loads(ors.read())
                open_src_scripts.close()
                open_raw_scripts.close()

    def test_handlers(self):
        # data
        js = JsMunger.from_network_and_html(self.html_scripts, self.src_scripts)
        print(js.data)


if __name__ == "__main__":
    unittest.main()
