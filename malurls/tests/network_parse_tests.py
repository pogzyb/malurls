# test cases
import unittest
import json
from malurls.lib.parsers.network_parse import Networked


class TestNetworkParse(unittest.TestCase):

    def setUp(self):
        print("Setting up...")
        try:
            f = open('./files_tests/network_test_data.json', 'r')
        except OSError:
            print("ERROR.")
        else:
            with f:
                data = f.read()
                self.network_data = json.loads(data)
                f.close()
                print("...setUp complete!")

    def test_from_data(self):
        network = Networked.from_data(self.network_data)
        print(network.data)

        # with open("./files_tests/network_parse_output.json", "w") as out:
            # out.write(json.dumps(munger.munged_data, indent=4, sort_keys=True, default=str))
