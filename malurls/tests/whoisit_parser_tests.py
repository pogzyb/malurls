import unittest
import json
from malurls.lib.parsers.whois_parse import WhoIsYou


class TestWhoIs(unittest.TestCase):

    def setUp(self):
        print("[ INFO ] Setting Up...")
        # test_data = open('./files_tests/debug_data_outputgithub.com.json', 'r')
        test_data = open('./files_tests/debug_data_outputdiaryofagameaddict.com.json', 'r')
        # data is a list of "cookie" dictionaries
        self.data = json.loads(test_data.read())["data"]["whois"]
        print(self.data)
        test_data.close()
        print("[ INFO ] Set up complete!  self.data is available.")

    def test_sanity_WhoIsYou_clsmethod(self):
        print("[ STARTING ] test_sanity_WhoIsYou_clsmethod")
        whois_parsed = WhoIsYou.from_data(self.data)
        print(whois_parsed.data)
        print("[ PASSED ] test_sanity_WhoIsYou_clsmethod")