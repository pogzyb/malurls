# My MongoDB interface
import pymongo
from malurls.lib.logger import LogSomething


# manage mongodb with python interface
class DatabaseManager(object):

    def __init__(self, database, collection):
        self.client = pymongo.MongoClient("mongodb://localhost:27017/")
        self.database = self.client[database]
        self.collection = self.database[collection]

    def insert_data(self, data):
        try:
            self.collection.insert_one(data, bypass_document_validation=True)
        except Exception as e:
            # log failure
            msg = f"[ ERROR ] Database insert failure!: {e}"
            print(msg)
            LogSomething.log(msg)
        else:
            return True

    def check_url_exists(self, url):
        query = {"submitted-url": url}
        found = self.collection.find(query)
        if found.count() > 0:
            return True
        else:
            return False

    def insert_many(self, data):
        # Log a db many entry
        self.collection.insert_many(data)
