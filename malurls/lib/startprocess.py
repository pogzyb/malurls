import subprocess
import os
from random import randrange


class BrowserProcess(object):

    def __init__(self):
        self.port = randrange(9000, 9999)
        self.subproc = None
        self.pgid = None
        self.running = False

    @classmethod
    def start_browser_process(cls, process):
        proc = BrowserProcess()
        proc.subproc = subprocess.Popen((process, f"{proc.port}"))
        proc.killgroup = os.getpgid(proc.subproc.pid)
        proc.running = True
        return proc

    def kill(self):
        if self.running:
            subprocess.Popen(
                ("pkill", "-9", "-g", f"{self.killgroup}"),
            )
            self.running = False
            print("[ PROCESS KILLED ]")
