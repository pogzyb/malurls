# Headless Chrome interface
from malurls.lib.utilities import *
from PyChromeDevTools import ChromeInterface
from malurls.lib.logger import LogSomething


class BrowserDriver(object):

    def __init__(self, port):
        # interface
        self.chrome = ChromeInterface(port=port)
        # enables
        self.chrome.Page.enable()
        self.chrome.Network.enable()
        self.chrome.Performance.enable()
        self.chrome.Runtime.enable()
        self.chrome.DOM.enable()
        # vars
        self.scripts_from_src = list()
        self.scripts_from_html = list()
        self.network = list()
        self.html = None

    @classmethod
    def start_and_navigate_to(cls, url, port):
        driver = BrowserDriver(port)
        try:
            print(f"[ INFO ] Navigating to: {url}")
            driver.chrome.Page.navigate(url=url, timeout=15)
        except Exception as e:
            LogSomething.log(f"BrowserDriver Navigation Error: {e} | {url}")
            sys.exit(1)
        else:
            # ensure cookies and cache are cleared for a clean run
            driver.chrome.Network.clearBrowserCache()
            driver.chrome.Network.clearBrowserCookies()
            # retrieve network events
            _, driver.network = driver.chrome.wait_event("Page.frameStoppedLoading", timeout=15)
            # pause for page safety
            print("[ INFO ] waiting for page to finish loading.")
            time.sleep(5)
            # grab the page's html at runtime
            driver.html = driver.chrome.Runtime.evaluate(
                expression="document.documentElement.innerHTML",
                awaitPromis=True
            )['result']['result']['value']
            return driver

    def add_browser_stats(self):
        browser_stats = dict()
        browser_stats['cookies'] = self.page_cookies()
        browser_stats['heap'] = self.page_heap_usage()
        browser_stats['metrics'] = self.page_metrics()
        return browser_stats

    def extract_script_links(self):
        scripts = list()
        for m in self.network:
            try:
                if (m["method"] == "Network.requestWillBeSent") and (m["params"]["type"] == "Script"):
                    src_url = m["params"]["request"]["url"]
                    # script_id fix for mongodb '.' key errors
                    script_id = m["params"]["requestId"]
                    scripts.append((script_id, src_url))
            except KeyError:
                LogSomething.log("[ ERROR ] No 'method' inside network log (driver).")
                continue
        return scripts

    def page_metrics(self):
        metrics = dict()
        m = self.chrome.Performance.getMetrics()
        for d in m["result"]["metrics"]:
            for key, value in d.items():
                metrics[key] = value
        return metrics

    def page_heap_usage(self):
        heap = self.chrome.Runtime.getHeapUsage()
        return heap["result"]

    def page_cookies(self):
        cookies = self.chrome.Page.getCookies()
        return cookies["result"]["cookies"]

    def page_download_behavior(self):
        # Page.setDownloadBehavior
        pass
