# URL utilities
import os.path
import sys
import requests
import base64
import whois
import json
import time
import datetime
from ipaddress import ip_address
from urllib.parse import urlparse


def open_ad_servers():
    try:
        # open ad-servers file and create global var
        with open("/Users/josepho/PycharmProjects/malurls/malurls/files/adservers.txt", "r") as ads:
            ads = ads.read().split("\n")
            return [hostname.replace("*", "") for hostname in ads]
    except OSError as e:
        # log failure and datetime
        print(f"failed open: {e}")
        sys.exit(1)


AD_SERVERS = open_ad_servers()


# ad server check
def is_ad_server(url):
    for ads in AD_SERVERS:
        if ads in url:
            return True
    return False


# make and return an id for a url
def make_id(url):
    str_id = url + str(time.time())
    return base64.b64encode(str_id.encode()).decode()


# open the keywords.json file and dump into attrs dict
def open_checks_conf():
    try:
        path = os.path.abspath("")
        conf_skeleton = open(path, "r")
    except Exception as e:
        # log error here
        print(f"error opening attrs: {e}")
        sys.exit(1)
    else:
        with conf_skeleton:
            attrs = json.loads(conf_skeleton.read())
            return attrs


# parse a url; return a dict
def get_urlparse(url):
    parsed = urlparse(url)
    return {
        "path": parsed.path,
        "protocol": parsed.scheme,
        "hostname": parsed.netloc
    }


# check whether the input string is a url
def is_ip(url):
    try:
        ip_address(url)
        return True
    except ValueError:
        return False


# return whois data dict
def get_whois(url):
    data = whois.whois(url)
    return data


# check if url can be reached
def initial_url_check(url):
    try:
        resp = requests.get(url, timeout=7)
        if resp.status_code == 200:
            return {
                "reachable": True,
                "is_redirect": resp.is_redirect,
                "redirects": [(r.url, r.status_code) for r in resp.history],
                "final-url": resp.url
            }
        else:
            return False
    except Exception as e:
        # log errors here
        print(f"[ UNREACHABLE ] {url} could not be reached: {e}")
        return False


# average length of a specified list
def avg_length(listof):
    return round(sum(map(len, listof)) / len(listof) if len(listof) > 0 else 0.0, 2)


# max length of a specified list
def max_length(listof):
    return max(map(len, listof)) if len(listof) > 0 else 0.0


# OLD CAN DELETE: open the metadata skeleton
# def open_metadata():
#     try:
#         meta_skeleton = open('./files/metadata.json', 'r')
#     except OSError as e:
#         # log failure and datetime
#         print(f"failed open: {e}")
#         sys.exit(1)
#     else:
#         with meta_skeleton:
#             metadata = json.loads(meta_skeleton.read())
#             return metadata

