from datetime import datetime
from malurls.lib.utilities import max_length, avg_length


class CookieJar(object):
    """
    Cookies dictionaries are already structured,
    and only need to be translated into self.data
    """

    def __init__(self):
        self.data = {
            "cookies-size-average": None,
            "cookies-size-total": None,
            "cookies-size-max": None,
            "cookies-num-secure-true": None,
            "cookies-num-session-true": None,
            "cookies-num-httpOnly-true": None,
            "cookies-value-max-length": None,
            "cookies-value-average-length": None,
            "cookies-value-num-integer": None,
            # "cookies-value-num-encoded": None,
            "cookies-name-max-length": None,
            "cookies-name-average-length": None,
            "cookies-num-cookies": None,
            "cookies-num-cookies-google": None,
            "cookies-num-unique-domains": None,
            "cookies-num-expire-within-10days": None,
            "cookies-num-expire-over-30days": None
        }

    @staticmethod
    def knead(cookies):
        d = dict()
        for c in cookies:
            for key, value in c.items():
                if key not in d:
                    d[key] = [value]
                else:
                    d[key].append(value)
        return d

    @staticmethod
    def try_convert(v):
        try:
            int(v)
            return 1
        except (ValueError, TypeError):
            return 0

    # returns a CookieJar object made from raw cookie data
    @classmethod
    def from_data(cls, cookie_dough):
        fresh_cookies = CookieJar()
        n_cookies = len(cookie_dough)
        if n_cookies == 0:
            return fresh_cookies
        fresh_cookies.data["cookies-num-cookies"] = n_cookies
        cookies = cls.knead(cookie_dough)
        max_and_avgs = ['name', 'value']
        for n in max_and_avgs:
            fresh_cookies.data["cookies-"+n+"-max-length"] = max_length(cookies[n])
            fresh_cookies.data["cookies-"+n+"-average-length"] = avg_length(cookies[n])
        # sizes
        sum_size = sum([int(x) for x in cookies["size"]])
        fresh_cookies.data["cookies-size-total"] = sum_size
        fresh_cookies.data["cookies-size-average"] = sum_size / len(cookies["size"])
        fresh_cookies.data["cookies-size-max"] = max(cookies["size"])
        # domains
        fresh_cookies.data["cookies-num-cookies-google"] = len(
            list(filter(lambda x: 'google' in x.lower(), cookies["domain"])))
        fresh_cookies.data["cookies-num-unique-domains"] = len(set(cookies["domain"]))
        # session, secure, and httpOnly
        fresh_cookies.data["cookies-num-secure-true"] = sum(cookies["secure"])
        fresh_cookies.data["cookies-num-session-true"] = sum(cookies["session"])
        fresh_cookies.data["cookies-num-httpOnly-true"] = sum(cookies["httpOnly"])
        # ints
        fresh_cookies.data["cookies-value-num-integer"] = sum(
            list(map(lambda x: CookieJar.try_convert(x), cookies["value"])))
        # encoded?
        # data["cookies-value-num-encoded"] = # apply is_encoded func
        # expires
        today = datetime.today()
        cookies["expires"] = list(map(lambda x: datetime.utcfromtimestamp(x), cookies["expires"]))
        fresh_cookies.data["cookies-num-expire-over-30days"] = len(
            list(filter(lambda x: (x - today).days >= 30, cookies["expires"])))
        fresh_cookies.data["cookies-num-expire-within-10days"] = len(
            list(filter(lambda x: (x - today).days <= 10, cookies["expires"])))
        return fresh_cookies



