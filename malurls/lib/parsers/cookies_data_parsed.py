from dataclasses import dataclass
from datetime import datetime
from malurls.lib.utilities import max_length, avg_length


@dataclass
class CookiesParser(object):
    """Parse cookies data and
     return structured data from browser"""
    cookiesAverageSize: float = 0
    cookiesTotalSize: float = 0
    cookiesMaxSize: int = 0
    cookiesNumSecure: int = 0
    cookiesNumSession: int = 0
    cookiesNumHttpOnly: int = 0
    cookiesAverageLen: float = 0
    cookiesMaxLen: int = 0
    cookiesNumCookies: int = 0
    cookiesNumGoogle: int = 0
    cookiesNumUniqueDomains: int = 0
    cookiesNumExpireUnder10Days: int = 0
    cookiesNumExpireOver30days: int = 0
    cookiesNumValueInteger: int = 0

    def __dict__(self):
        pass

    @staticmethod
    def knead(cookies):
        d = dict()
        for c in cookies:
            for key, value in c.items():
                if key not in d:
                    d[key] = [value]
                else:
                    d[key].append(value)
        return d

    @staticmethod
    def try_convert(v):
        try:
            int(v)
            return 1
        except (ValueError, TypeError):
            return 0

    # returns a CookieJar object made from raw cookie data
    @classmethod
    def from_data(cls, cookie_dough):
        fresh_cookies = CookiesParser()
        n_cookies = len(cookie_dough)
        if n_cookies == 0:
            return fresh_cookies
        fresh_cookies.data["cookies-num-cookies"] = n_cookies
        cookies = cls.knead(cookie_dough)
        max_and_avgs = ['name', 'value']
        for n in max_and_avgs:
            fresh_cookies.data["cookies-" + n + "-max-length"] = max_length(cookies[n])
            fresh_cookies.data["cookies-" + n + "-average-length"] = avg_length(cookies[n])
        # sizes
        sum_size = sum([int(x) for x in cookies["size"]])
        fresh_cookies.cookiesTotalSize = sum_size
        fresh_cookies.cookiesAverageSize = sum_size / len(cookies["size"])
        fresh_cookies.cookiesMaxSize = max(cookies["size"])
        # domains
        fresh_cookies.data["cookies-num-cookies-google"] = len(
            list(filter(lambda x: 'google' in x.lower(), cookies["domain"])))
        fresh_cookies.data["cookies-num-unique-domains"] = len(set(cookies["domain"]))
        # session, secure, and httpOnly
        fresh_cookies.data["cookies-num-secure-true"] = sum(cookies["secure"])
        fresh_cookies.data["cookies-num-session-true"] = sum(cookies["session"])
        fresh_cookies.data["cookies-num-httpOnly-true"] = sum(cookies["httpOnly"])
        # ints
        fresh_cookies.data["cookies-value-num-integer"] = sum(
            list(map(lambda x: cls.try_convert(x), cookies["value"])))
        # encoded?
        # data["cookies-value-num-encoded"] = # apply is_encoded func
        # expires
        today = datetime.today()
        cookies["expires"] = list(map(lambda x: datetime.utcfromtimestamp(x), cookies["expires"]))
        fresh_cookies.data["cookies-num-expire-over-30days"] = len(
            list(filter(lambda x: (x - today).days >= 30, cookies["expires"])))
        fresh_cookies.data["cookies-num-expire-within-10days"] = len(
            list(filter(lambda x: (x - today).days <= 10, cookies["expires"])))
        return fresh_cookies

