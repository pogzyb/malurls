import tldextract
from malurls.lib.utilities import is_ip
import re


class URLFeatures(object):

    def __init__(self):
        self.tld_extractor = tldextract.TLDExtract(cache_file=False)
        self.data = {
            "url-is-ip": None,
            # "url-is-redirect": None,
            "url-num-redirects": None,
            "url-protocol": None,
            "url-path-length": None,
            "url-num-sub-domains": None,
            "url-path-num-slashes": None,
            "url-path-dot-ending": None,
            "url-num-special-chars": None,
            "url-top-level-domain": None
            # "url-encoded": None
        }

    @staticmethod
    def check_for_file_ending(path):
        matches = [
            '.php', '.exec', '.exe', '.pdf', '.gif', '.txt', '.html', '.jpeg',
            '.asp', '.aspx', '.htm', '.cgi', '.shtml', '.pl', '.jpg', '.png'
        ]
        for ending in matches:
            if re.search(ending+'$', path):
                return ending
        return None

    # todo: encoding, check for script tags, other js funcs
    @classmethod
    def from_data(cls, url_parsed_data, valid_data):
        urlf = URLFeatures()
        # extract top level domain and sub-domain info
        url_tld_info = urlf.tld_extractor(url_parsed_data["hostname"])
        # add domain features
        urlf.data["url-protocol"] = url_parsed_data["protocol"]
        urlf.data["url-num-sub-domains"] = len(url_tld_info.subdomain.split("."))
        urlf.data["url-top-level-domain"] = url_tld_info.suffix
        # other validation features
        urlf.data["url-is-ip"] = int(is_ip(url_parsed_data["hostname"]))
        # urlf.data["url-is-redirect"] = int(valid_data["is_redirect"])
        urlf.data["url-num-redirects"] = len(valid_data["redirects"])
        if url_parsed_data["path"] != "/":
            # add path features
            urlf.data["url-path-length"] = len(url_parsed_data["path"])
            urlf.data["url-path-num-slashes"] = len(url_parsed_data["path"].split("/")) - 1
            urlf.data["url-num-special-chars"] = \
                len([x for x in url_parsed_data["path"] if not x.isalnum()]) - urlf.data["url-path-num-slashes"]
            urlf.data["url-path-dot-ending"] = cls.check_for_file_ending(url_parsed_data["path"])

        return urlf

