class BaseParser(object):

    conf = {
        "javascript": {
            "functions": [
                "add", "charAt", "charCodeAt", "call", "concat", "escape", "unescape", "replace", "includes",
                "indexOf", "join", "link", "map", "max", "min", "of", "parse", "push", "pop", "raw",
                "reduce", "return", "reverse", "search", "set", "slice", "sort", "stringify", "sub",
                "substr", "substring", "test", "throw", "toFixed", "toLocaleString", "toLowerCase",
                "toSource", "toString", "toUpperCase", "trim", "trimEnd", "trimStart", "trunc", "valueOf",
                "values", "xor", "and", "apply", "assign", "copyWithin", "create", "delete", "defineProperty",
                "endsWith", "entries", "every", "exchange", "exec", "fill", "filter", "find", "findIndex",
                "flatMap", "fromCharCode", "fromCodePoint", "grow", "has", "is", "isArray", "isInteger", "isNan",
                "isProtoTypeOf", "isView", "isSealed", "keys", "lastIndexOf", "lessThan", "load", "run", "split",
                "length", "round", "append", "onload", "onerror"
            ]
        },
        "html": {
            "srcElements": [
                "audio", "embed", "iframe", "img", "input", "script", "source", "track", "video"
            ],
            "hrefElements": [
                "a", "area", "base", "link"
            ],
            "keyElements": [
                "head", "body", "p", "a", "div", "video", "form", "iframe", "embed", "input",
                "button", "header", "footer", "frame", "nav", "textarea", "li", "source", "script"
            ],
            "keyEvents": {
                "window_events": [
                    "onafterprint", "onbeforeprint", "onbeforeunload", "onerror",
                    "onhashchange", "onload", "onoffline", "ononline", "onpagehide", "onpageshow",
                    "onpopstate", "onstorage", "onresize", "onunload", "onunload", "onbeforeload"
                ],
                "form_events": [
                    "onblur", "onchange", "oncontextmenu", "onfocus", "oninput", "oninvalid",
                    "onreset", "onsearch", "onselect", "onsubmit", "onforminput"
                ],
                "keyboard_events": [
                    "onkeydown", "onkeypress", "onkeyup"
                ],
                "mouse_events": [
                    "onclick", "ondblclick", "onmousedown", "onmousemove", "onmouseout",
                    "onmouseover", "onmouseup", "onmousewheel", "onwheel"
                ],
                "drag_events": [
                    "ondrag", "ondragend", "ondragenter", "ondragleave", "ondragover",
                    "ondragstart", "ondrop", "onscroll"
                ],
                "clipboard_events": [
                    "oncopy", "oncut", "onpaste"
                ],
                "media_events": [
                    "onabort", "oncanplay", "oncanplaythrough", "oncuechange",
                    "ondurationchange", "onloadstart", "onprogress", "onplay", "onwaiting"
                ],
                "misc_events": [
                    "ontoggle"
                ]
            }
        }
    }

    def __init__(self):
        self.data = dict()

    # 2 keys list value
    def add_to_data_list(self, keys, item):
        k1, k2 = keys
        try:
            self.data[k1][k2].append(item)
        except KeyError:
            try:
                self.data[k1][k2] = [item]
            except KeyError:
                self.data[k1] = {
                    k2: [item]
                }

    # 2 keys single item
    def add_to_data_alone(self, keys, item):
        k1, k2 = keys
        try:
            self.data[k1][k2] = item
        except KeyError:
            self.data[k1] = {
                k2: item
            }

    # def add_to_data(self, key, item):

