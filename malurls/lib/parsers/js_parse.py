# JavaScript Parser
import re
import base64
import requests
from malurls.lib.parsers.baseparser import BaseParser
from malurls.lib.utilities import is_ad_server
from malurls.lib.logger import LogSomething
from malurls.lib.utilities import avg_length, max_length


class JsMunger(BaseParser):

    def __init__(self):
        super().__init__()
        self.doc_regex = "document\.(\w+)"
        self.var_regex = "var\s(\w+)\s?=\s?([^;]+);"
        # javascript return features after data munging
        self.munged_data = {
            "js-num-scripts": 0,
            "js-num-ad-scripts": 0,
            "js-total-num-variables": 0,
            "js-multi-dec-variables": 0,
        }

    @classmethod
    def from_network_and_html(cls, html_scripts, src_scripts):
        js = JsMunger()
        # init these
        js.data["ad_server_links"] = dict()
        js.data["script_on_page"] = dict()
        # add scripts from src into data dict
        # Note: must encode the ids and links to avoid mongodb errors
        for id_script_pair in src_scripts:
            # get id and src link
            i, link = id_script_pair[0], id_script_pair[1]
            # encode id
            i = base64.b64encode(i.encode()).decode('utf-8')
            # check link is not an adserver script
            if not is_ad_server(link):
                src_script = cls.load_src_script(link)
                # encode link before adding
                link = base64.b64encode(link.encode()).decode('utf-8')
                js.add_to_data_alone(keys=(i, link), item=src_script)
            else:
                # if adserver; add to data under different key
                js.add_to_data_alone(keys=("ad_server_links", i), item=link)
        # add scripts from html into data dict
        for ind, page_script in enumerate(html_scripts):
            key = "script_on_page"  # change var name?
            js.add_to_data_alone(keys=(key, str(ind)), item=page_script)
        # done; return class instance
        return js

    @staticmethod
    def load_src_script(link):
        try:
            resp = requests.get(link, timeout=2)
            return resp.text
        except ConnectionError as e:
            LogSomething.log(f"Error: {e} | loading script from src: {link}")
            pass

    # return dataset features
    def extract_from_data(self):

        self.munged_data["js-avg-len-var"] = list()
        self.munged_data["js-avg-len-var-value"] = list()

        src_scripts = [s for s in self.data.keys() if s not in ['ad_server_links', 'script_on_page']]

        self.munged_data["js-num-scripts"] = sum([len(self.data[k]) for k in self.data])
        self.munged_data["js-num-scripts-from-html"] = len(self.data['script_on_page'])

        # src script features
        for s in src_scripts:
            for i, script in self.data[s].items():
                self.handle_script(script)

        # html script features
        for _, script in self.data["script_on_page"].items():
            self.handle_script(script)

        # avg and max
        self.munged_data["js-max-len-var"] = max_length(self.munged_data["js-avg-len-var"])
        self.munged_data["js-max-len-var-value"] = max_length(self.munged_data["js-avg-len-var-value"])
        self.munged_data["js-avg-len-var"] = avg_length(self.munged_data["js-avg-len-var"])
        self.munged_data["js-avg-len-var-value"] = avg_length(self.munged_data["js-avg-len-var-value"])

        # ad server script features
        self.munged_data["js-num-ad-scripts"] = len(self.data["ad_server_links"])

        return self.munged_data

    # add/increment individual script attributes to munged_data
    def handle_script(self, script):
        # find standard functions
        self.munged_data["js-avg-len-var"] = list()
        self.munged_data["js-avg-len-var-value"] = list()
        for func in self.conf["javascript"]["functions"]:
            functions = re.findall(func, script, re.DOTALL)
            key = "js-" + func
            if key not in self.munged_data:
                self.munged_data[key] = len(functions)
            else:
                self.munged_data[key] += len(functions)
        # document. and variable declarations
        documents = re.findall(self.doc_regex, script, re.DOTALL)
        for d in documents:
            key = "js-document-" + d
            if key not in self.munged_data:
                self.munged_data[key] = 1
            else:
                self.munged_data[key] += 1
        # variables get special treatment; want to track lengths and value lengths
        variables = re.findall(self.var_regex, script, re.DOTALL)
        for v in variables:
            many = len(v[1].split(","))
            if many > 1:
                self.munged_data["js-multi-dec-variables"] += 1
            self.munged_data["js-total-num-variables"] += many
            self.munged_data["js-avg-len-var"].append(v[0])
            self.munged_data["js-avg-len-var-value"].append(v[1])


