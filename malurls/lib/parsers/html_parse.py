from html.parser import HTMLParser
from malurls.lib.parsers.baseparser import BaseParser
from malurls.lib.detectors import href_and_src_check, iframe_check
from malurls.lib.utilities import avg_length, max_length


# inherits from HTMLParser for my needs
class HtmlMunger(BaseParser, HTMLParser):

    def __init__(self):
        BaseParser.__init__(self)
        HTMLParser.__init__(self)
        self.scripts_from_html = list()
        self.check_script = False
        # html return features after data munging
        self.munged_data = {}
        #     "html-length-no-spaces": None,
        #         #     "html-num-tags": None,
        #         #     "html-num-href": None,
        #         #     "html-num-src": None,
        #         #     # ...
        #         # }

    @classmethod
    def from_html(cls, html):
        munged = HtmlMunger()
        munged.feed(html)
        return munged

    def handle_starttag(self, tag, attrs):
        # check for full script on page
        if (tag == "script") and ('src' not in [v[0] for v in attrs]):
            self.check_script = True
        # organize into data dict
        for attr, attr_value in attrs:
            attr = "data-*" if "data-" in attr else attr
            attr_value = attr_value if attr_value else ""
            self.add_to_data_list(keys=(tag, attr), item=attr_value)

    def handle_data(self, data):
        # append scripts to list
        if self.check_script:
            self.scripts_from_html.append(data)
            self.check_script = False

    def handle_comment(self, data):
        self.add_to_data_list(keys=("misc", "comment"), item=data)

    def handle_decl(self, data):
        self.add_to_data_list(keys=("misc", "declaration"), item=data)

    # parse raw html into manageable chunks which can be processed here
    def extract_from_data(self):
        # generic stats: all tags
        self.munged_data["num-unique-tags"] = len(self.data)
        self.munged_data["num-total-tags"] = sum([len(self.data[l].values()) for l in self.data])

        for tag in self.data:
            # add checks for href and src
            if (tag in self.conf["html"]["hrefElements"]) or (tag in self.conf["html"]["srcElements"]):
                keys = self.data[tag].keys()
                if "href" in keys:
                    # pass list of elements
                    self.munged_data.update(href_and_src_check(tag, "href", self.data[tag]["href"]))
                if "src" in keys:
                    self.munged_data.update(href_and_src_check(tag, "src", self.data[tag]["src"]))

            # if tag == "iframe":
                # special checks for iframes!
                # self.munged_data.update(iframe_check(self.data[tag]))

            # todo: checks for style?
            # if tag in self.conf["styleElements"]: >>> would need to basically check everything...

            # generic stats for every tag and attribute pair
            for attr in self.data[tag]:
                self.munged_data["total-num-" + attr + "-" + tag] = len(self.data[tag][attr])
                uni = set(self.data[tag][attr])
                self.munged_data["unique-num-" + attr + "-" + tag] = len(uni)
                self.munged_data["avg-len-" + attr + "-" + tag] = avg_length(uni)
                self.munged_data["max-len-" + attr + "-" + tag] = max_length(uni)
        return self.munged_data
