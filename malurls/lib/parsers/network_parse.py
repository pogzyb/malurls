import email.utils as eut
from datetime import datetime
from collections import Counter
from malurls.lib.utilities import is_ad_server
from malurls.lib.logger import LogSomething
import re
import os


class Networked(object):
    """
    Network tab of the browser. For now, only non-ad-server scripts requests are being checked.
    The return data is semi-structured -> headers are one-hot-encoded, key-values are also one-hot-encoded.
    """

    path = os.path.join(os.getcwd(), "malurls/files/http_headers.txt")
    with open(path, "r") as f:
        http_headers = [line.rstrip() for line in f]

    cache_types = [
        'public',
        'private',
        'no-store',
        'must-revalidate',
        'no-transform',
        'only-if-cached',
        'min-fresh',
        'max-stale',
        'immutable',
        'stale-while-revalidate',
        'stale-if-error'
    ]

    def __init__(self):
        self.data = {
            "max-num-resp-headers": None,
            "max-num-req-headers": None,
        }

    @staticmethod
    def handle_cache_controls(caches):
        ages = list()
        args = list()
        for c in caches:
            # find age (numbers)
            age = re.search(r'\d+', c)
            if age:
                ages.append(int(age.group()))
            # find other args
            for c_type in Networked.cache_types:
                if c_type in c.lower():
                    args.append(c_type)
        return ages, args

    @classmethod
    def from_data(cls, data):
        today = datetime.now()
        net = Networked()
        network_resp_recvd = dict()
        methods = list()

        # clean; only keep 'responseReceived' for scripts
        for msg in data:
            try:
                if msg["method"] == "Network.responseReceived" and \
                        msg["params"]["type"] == 'Script' and \
                        not is_ad_server(msg['params']['response']['url']):
                    network_resp_recvd[msg["params"]["requestId"]] = msg["params"]
                elif msg["method"] == "Network.requestWillBeSent" and \
                        msg["params"]["type"] == 'Script' and \
                        not is_ad_server(msg['params']['request']['url']):
                    request_method = msg['params']['request']['method']
                    methods.append(request_method)
            except KeyError as e:
                LogSomething.log("[ ERROR ] No 'method' key")
                continue

        if len(network_resp_recvd) > 0:
            # aggregation variables
            times = {
                'sent-recv-time': [],
                'dns-resolve-time': [],
                'connect-time': [],
                'send-time': [],
                'ssl-time': [],
                'worker-time': [],
                'receive-headers-end': [],
                'push-time': [],
            }
            categorical = {
                'content-type': [],
                'server': [],
                'accept-ranges': [],
                'content-encoding': [],
                'accept': [],
                # 'referer': [], # todo: special check same as OG url?
                'protocol': [],
            }
            numeric = {
                'expires': [],
                'last-modified': [],
                'content-length': []
            }
            misc_response_cats = {
                'remoteIPAddress': [],
                'remotePort': [],
                'securityState': [],
                'hadSecurityDetails': [],
            }
            special = {
                'cache-control': [],
                'cache-max-ages': []
            }

            # vars used for keeping track of headers
            usable_resp_header_keys = list()
            num_unusual_resp_header_keys = list()
            length_resp_headers = list()

            usable_req_header_keys = list()
            num_unusual_req_header_keys = list()
            length_req_headers = list()

            # AGGREGATIONS
            for msg in network_resp_recvd:
                # add misc response vars
                misc_response_cats['remoteIPAddress'].append(network_resp_recvd[msg]["response"]['remoteIPAddress'])
                misc_response_cats['remotePort'].append(network_resp_recvd[msg]["response"]['remotePort'])
                misc_response_cats['securityState'].append(network_resp_recvd[msg]["response"]['securityState'])
                if 'securityDetails' in network_resp_recvd[msg]["response"].keys():
                    misc_response_cats['hadSecurityDetails'].append(True)
                # timings
                timings = network_resp_recvd[msg]['response']['timing']
                # append times
                times['sent-recv-time'].append(network_resp_recvd[msg]['timestamp'] - timings['requestTime'])
                times['dns-resolve-time'].append(timings['dnsEnd'] - timings['dnsStart'])
                times['connect-time'].append(timings['connectEnd'] - timings['connectStart'])
                times['send-time'].append(timings['sendEnd'] - timings['sendStart'])
                times['ssl-time'].append(timings['sslEnd'] - timings['sslStart'])
                times['worker-time'].append(timings['workerReady'] - timings['workerStart'])
                times['receive-headers-end'].append(timings['receiveHeadersEnd'])
                times['push-time'].append(timings['pushEnd'] - timings['pushStart'])
                # handle response headers
                len_resp_headers = len(network_resp_recvd[msg]["response"]["headers"])
                length_resp_headers.append(len_resp_headers)
                resp_headers = {k.lower(): v for k, v in network_resp_recvd[msg]["response"]["headers"].items()}
                found_resp_headers = list(set(Networked.http_headers) & set(resp_headers.keys()))
                usable_resp_header_keys.extend(found_resp_headers)
                num_unusual_resp_header_keys.append(len_resp_headers - len(found_resp_headers))

                for key in found_resp_headers:
                    # update counts
                    n_key = "num-resp-key-" + key
                    if n_key not in net.data:
                        net.data[n_key] = 1
                    else:
                        net.data[n_key] += 1
                    # add to correct computation dictionary
                    if key in categorical:
                        categorical[key].append(resp_headers[key])
                    elif key in numeric:
                        numeric[key].append(resp_headers[key])
                    elif key == 'cache-control':
                        special[key].append(resp_headers[key])

                # requestHeaders not always present
                try:
                    len_req_headers = len(network_resp_recvd[msg]["response"]["requestHeaders"])
                    length_req_headers.append(len_req_headers)
                    req_headers = {k.lower(): v for k, v in network_resp_recvd[msg]["response"]["requestHeaders"].items()}
                    found_req_headers = list(set(Networked.http_headers) & set(req_headers.keys()))
                    usable_req_header_keys.extend(found_req_headers)
                    num_unusual_req_header_keys.append(len_req_headers - len(found_req_headers))

                    for key in found_req_headers:
                        # update counts
                        n_key = "num-req-key-" + key
                        if n_key not in net.data:
                            net.data[n_key] = 1
                        else:
                            net.data[n_key] += 1
                        # add to correct computation dictionary
                        if key in categorical:
                            categorical[key.lower()].append(req_headers[key])
                        elif key in numeric:
                            numeric[key.lower()].append(req_headers[key])
                        elif key == 'cache-control':
                            special[key].append(req_headers[key])

                except KeyError:
                    pass

            # TRANSFORMATIONS
            for t in times:
                net.data["net-max-" + t] = max(times[t])
                net.data["net-avg-" + t] = round((sum(times[t]) / len(times[t])), 4)

            # stats
            net.data["max-num-resp-headers"] = max(length_resp_headers)
            net.data["max-num-req-headers"] = max(length_req_headers)
            net.data["num-unusual-resp-headers"] = sum(num_unusual_resp_header_keys)
            net.data["num-unusual-req-headers"] = sum(num_unusual_req_header_keys)

            # numeric transformations
            if len(numeric['expires']) > 0:
                exp_days = list()
                for n in numeric['expires']:
                    try:
                        d = datetime(*eut.parsedate(n)[:6])
                        exp_days.append((today - d).days)
                    except TypeError:
                        try:
                            exp_days.append(int(n))
                        except ValueError:
                            continue
                net.data["net-max-resp-expires"] = max(exp_days)
                net.data["net-avg-resp-expires"] = round((sum(exp_days) / len(exp_days)), 4)

            if len(numeric['last-modified']) > 0:
                days = [(today - datetime(*eut.parsedate(x)[:6])).days for x in numeric["last-modified"]]
                net.data["net-max-days-last-modified"] = max(days)
                net.data["net-avg-days-last-modified"] = round((sum(days) / len(days)), 4)

            if len(numeric['content-length']) > 0:
                cts = [int(x) for x in numeric['content-length']]
                net.data["net-max-content-length"] = max(cts)
                net.data["net-avg-content-length"] = round((sum(cts) / len(cts)), 4)

            # categorical transformations
            for c in categorical:
                counts = Counter(categorical[c])
                for val, num in counts.items():
                    if '.' in val:
                        val = val.replace('.', '_')
                    net.data["net-" + c + "-" + val] = num

            # misc transformations
            net.data["num-set-remoteIPAddresses"] = len(set(misc_response_cats['remoteIPAddress']))
            net.data["num-hadSecurityDetails"] = sum(misc_response_cats['hadSecurityDetails'])
            counts = Counter(misc_response_cats['remotePort'])
            for val, num in counts.items():
                net.data["num-remote-port-" + str(val)] = num

            # methods
            counts = Counter(methods)
            for val, num in counts.items():
                net.data["num-request-method-" + val] = num

            # caches
            ages, args = cls.handle_cache_controls(special['cache-control'])
            leng_ages = len(ages)
            net.data["max-cache-control-age"] = max(ages) if leng_ages > 0 else 0
            net.data["avg-cache-control-age"] = round(sum(ages) / len(ages), 4) if leng_ages > 0 else 0
            counts = Counter(args)
            for val, num in counts.items():
                net.data["num-cache-control-" + val] = num

        else:
            # log that there was nothing.
            pass

        return net


