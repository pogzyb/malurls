from datetime import datetime
import re


class WhoIsYou(object):
    """
    Whois data dictionaries vary url to url.
    Special transforms are needed to translate data
    into the self.data attribute.
    """

    top_registrars = [
        'godaddy', 'bluehost', 'name.',
        'hostgator', '1&1', 'register',
        'domains', 'namecheap'
    ]

    date_keys = [
        ('expiration_date', 'whois-days-until-expires'),
        ('updated_date', 'whois-days-since-updated'),
        ('creation_date', 'whois-days-since-created')
    ]

    def __init__(self):
        self.data = {
            "whois-num-keys": None,
            "whois-days-since-created": None,
            "whois-days-since-updated": None,
            "whois-days-until-expires": None,
            "whois-num-updated-dates": None,
            "whois-number-of-nameservers": None,
            # "whois-status-is-prohibited": None,
            "whois-dnssec-signed": None,
            "whois-country": None,
            # "whois-has-state": None,
            # "whois-has-city": None,
            # "whois-has-address": None,
            # "whois-has-zipcode": None,
            "whois-has-org": None
        }

    # find keys based on regex (most whois have some things in common)
    # keys wanted: country, zipcode, name-servers, creation_date, updated_date, expiration_date
    @staticmethod
    def handle_keys(keys):
        # search for the keys; if can't find em check for the regexes in the list
        # values are lists of possible alternate names for the keys
        keys_finder = {
            "creation_date": ["\s(\w*creat\w*)\s"],
            "updated_date": ["\s(\w*update\w*)\s", "\s(\w*modif\w*)\s", "\s(\w*change\w*)\s"],
            "expiration_date": ["\s(\w*expir\w*)\s"],
            "country": ["\s(\w*country\w*)\s"],
            # "zipcode": ["\s(\w*code\w*)\s"],
            "name_servers": ["\s(\w*server\w*)\s"],
            "status": ["status"],
            "dnssec": ["dnssec"],
            "org": ["\s(\w*owner\w*)\s"],
            "registrar": ["\s(\w*regis\w*)\s"]
        }
        # return keys dict
        structured_keys = {
            "creation_date": None,
            "updated_date": None,
            "expiration_date": None,
            "country": None,
            # "zipcode": None,
            "name_servers": None,
            "status": None,
            "dnssec": None,
            "org": None,
            "registrar": None
        }
        found = [k for k in keys if k in keys_finder.keys()]
        for found_key in found:
            structured_keys[found_key] = found_key
        if len(found) != len(keys_finder):
            missing = [k for k in keys_finder if k not in found]
            for key in missing:
                for regex in keys_finder[key]:
                    match = re.search(regex, " ".join(keys))
                    if match and match.group(1) in keys:
                        structured_keys[key] = match.group(1)
                        break
        # return list of usable keys
        # if a key of keys_finder is found in input keys,
        # it is added to return dict { "keys_finder key": "key found in keys", }
        return structured_keys

    @classmethod
    def from_data(cls, who):
        whoops = WhoIsYou()
        whoops.data["whois-num-keys"] = len(who)
        # handle keys
        structured_keys = cls.handle_keys(who.keys())
        # check structured_keys for what was found
        for final_key, whois_key in structured_keys.items():
            if whois_key:
                who[final_key] = who[whois_key]
            else:
                who[final_key] = None
                # begin transformations
        # todo: More complex checks will come with better EDA
        whoops.data["whois-number-of-nameservers"] = len(who["name_servers"]) if who["name_servers"] else 0
        whoops.data["whois-number-of-statuses"] = len(who["status"]) if type(who["status"]) == list else 1
        # whoops.data["whois-status-is-ok"] = 1 if who["status"] and "ok" in who["status"].lower() else 0
        whoops.data["whois-has-org"] = 1 if who["org"] else 0
        whoops.data["whois-country"] = who["country"]
        # whoops.data["whois-zipcode"] = who["zipcode"]
        whoops.data["whois-dnssec-signed"] = cls.handle_dns(who["dnssec"]) if who["dnssec"] else None
        whoops.data["whois-top-ten-registars"] = cls.handle_registrars(who["registrar"]) if who["registrar"] else None
        whoops.data["whois-num-updated-dates"] = len(who["updated_date"]) if type(who["updated_date"]) == list else 1
        for d, i in cls.date_keys:
            n, days = cls.handle_dates(who[d])
            whoops.data[i] = n
            whoops.data[i] = days
        return whoops

    @staticmethod
    def handle_dns(dns):
        if type(dns) == list:
            dns = list(set([x.lower() for x in dns]))
            return dns[0]
        return dns

    @staticmethod
    def handle_registrars(regstr):
        for rg in WhoIsYou.top_registrars:
            if rg in regstr.lower():
                return 1
        return 0

    @staticmethod
    def handle_dates(date):
        today = datetime.today()
        if type(date) == list:
            return len(date), (today - date[0]).days
        else:
            return 0, None

    @staticmethod
    def date_helper(d):
        return datetime.strptime(d.split(".")[0], "%Y-%m-%dT%H:%M:%S")

    @staticmethod
    def handle_dates_json(date):
        today = datetime.today()
        # see if it conforms to the normal whois dates format
        if type(date) == list:
            if type(date[0]) == dict:
                d = WhoIsYou.date_helper(date[0]['$date'])
                return len(date), (today - d).days
            # weird date format encountered!
            else:
                # todo: do something about this?
                return None, None
        elif type(date) == dict:
            d = WhoIsYou.date_helper(date['$date'])
            return 1, (today - d).days
        # weird date format encountered!
        else:
            # todo: do something about this?
            return None, None
