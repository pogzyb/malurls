import re
import base64
import html
from urllib.parse import urlparse, unquote
from malurls.lib.utilities import is_ip

# check for events in single initial html tag: <div onload="function()" .. >
# def event_check(elements):
#     # loop through list of window events
#     for events in attrs["html"]["keyEvents"]:
#         for e in attrs["html"]["keyEvents"][events]:
#             regex = e + "=[\'\"][^\"\']+[\"\']"
#             check = re.search(regex, elem, re.IGNORECASE)
#             if check:
#                 action = check.group(1)
#                 # add to data hash
#                 add_to_data(keys=("event", tag + "-" + e), item=action)


# check for malicious href or src elements:
# - href="https://wrwer.com/javascript:alert()"
# - src=http://evil.org/malicious.js\x3e\x3c/script\x3e'
def href_and_src_check(tag, attr, elements):
    results_dict = {
        tag + "-" + attr + "-ab-path": 0,
        tag + "-" + attr + "-js-check": 0,
        tag + "-" + attr + "-script-check": 0,
        tag + "-" + attr + "-encodings-check": 0,
        tag + "-" + attr + "-ip-addr-path": 0
    }
    # individual checks
    for elem in elements:
        # increment dict with checks
        results_dict[tag + "-" + attr + "-ab-path"] += check_abs_path(elem)
        results_dict[tag + "-" + attr + "-js-check"] += check_javascript(elem)
        results_dict[tag + "-" + attr + "-script-check"] += check_script(elem)
        results_dict[tag + "-" + attr + "-encodings-check"] += check_encodings(elem)
        results_dict[tag + "-" + attr + "-ip-addr-path"] += is_ip(elem)

    return results_dict


# Suspicious HTML Feature Checks
# TODO: iframe special case: check height and width
def iframe_check(elem):
    pass


def check_abs_path(link):
    is_abs = bool(urlparse(link).netloc)
    if is_abs:
        return True
    return False


def check_javascript(link):
    check = re.search(r"javascript:", link, re.IGNORECASE)
    if check:
        return True
    return False


def check_script(link):
    check = re.search(r"script", link, re.IGNORECASE)
    if check:
        return True
    return False


# TODO: actual check on encoded instances! if url != decoded(url)
def check_encodings(link):
    encoding_pairs = [
        ("html-escape", html.unescape),
        # ("base64-decode", base64.b64decode),
        ("url-unquote", unquote),
        # ("hex-decode", str)
    ]
    # print(f"ORIGINAL LINK: {link}")
    for key, func in encoding_pairs:
        try:
            decoded = func(link)
            if decoded != link:
                # print(key, decoded)
                return True
        except Exception as e:
            print(f"decode key: {key} | error: {e}")
            continue
    return False
