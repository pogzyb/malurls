#!/usr/bin/python
import pika
from random import randrange
from malurls.lib.startprocess import BrowserProcess
from malurls.lib.logger import LogSomething
from malurls.lib.dbmanager import DatabaseManager
from malurls.lib.driver import BrowserDriver
from malurls.lib.parsers.html_parse import HtmlMunger
from malurls.lib.parsers.js_parse import JsMunger
from malurls.lib.parsers.cookies_parse import CookieJar
from malurls.lib.parsers.whois_parse import WhoIsYou
from malurls.lib.parsers.network_parse import Networked
from malurls.lib.parsers.url_parse import URLFeatures
from malurls.lib.utilities import *
import gc


# receive messages from queue
def receive(ch, method, properties, body):
    # received the message!
    url = body.decode()
    # start headless chrome
    process = BrowserProcess.start_browser_process("./browser.sh")
    # collect the data
    try:
        print(f"Collecting data for {url}")
        collector(url, process.port, 0, debug=False)
    except Exception as e:
        # error handling and logging
        LogSomething.log(f"Error: {e}")
    finally:
        # ensure headless chrome is killed
        process.kill()
        # log the finish
        LogSomething.log(f"Program finished in {round(time.time() - start, 2)} seconds")


# collect data and build the dataset for each URL
def collector(url, port, target=None, debug=False):

    # database connections
    # todo: read these names in from outside
    db_name = "mal-url-detection"
    col_metadata = "raw-url-data"
    raw_url_database = DatabaseManager(db_name, col_metadata)
    col_dataset = "munged-url-data"
    munged_url_database = DatabaseManager(db_name, col_dataset)

    # check db for url
    exists = raw_url_database.check_url_exists(url)
    if exists and not debug:
        LogSomething.log(f"[ INFO ] URL: {url} - ALREADY EXISTS in {col_metadata} collection")
        # sys.exit(1)
        return

    # initial url checks
    valid = initial_url_check(url)
    if not valid:
        LogSomething.log(f"[ INFO ] URL: {url} - FAILED initial URL validation checks!")
        # sys.exit(1)
        return

    # debug announcement
    elif debug:
        print(f"[ DEBUG MODE ] URL: {url} - PASSED initial URL validation checks!")

    # unique id for db insert
    # uid = make_id(url)

    # sleep to avoid connection refusal
    print("[ INFO ] Pausing for safety...")
    time.sleep(randrange(2, 5))

    # headless browser
    browser = BrowserDriver.start_and_navigate_to(url, port)

    # html and js feature extraction
    html = HtmlMunger.from_html(browser.html)
    js = JsMunger.from_network_and_html(
        html.scripts_from_html,  # list of scripts found within the page's html
        browser.extract_script_links()  # list of src script links loaded by the page
    )

    print("[ INFO ] Constructing metadata!")
    # construct metadata
    metadata = {
        "url": url,
        "timestamp": str(datetime.datetime.now()),
        "validations": valid,
        "data": {
            "whois": get_whois(url),
            "url-parsed": get_urlparse(url),
            "network": browser.network,
            "browser": browser.add_browser_stats(),
            "html": browser.html,
            "js": js.data
        }
    }

    # the url's disposition
    if target:
        metadata['target'] = target

    # debug: output to json file
    if debug:
        f = "debug_data_output"+metadata["data"]["url-parsed"]["hostname"]+".json"
        with open("./malurls/tests/files_tests/"+f, "w") as out:
            out.write(json.dumps(metadata, indent=4, sort_keys=True, default=str))
            print("[ DEBUG MODE ] Successful data COLLECTION! DID NOT SAVE TO DATABASE")
            print(f"[ DEBUG MODE ] Wrote data out to file: {f}")
    # else save to db
    else:
        # insert metadata
        inserted = raw_url_database.insert_data(metadata)
        if inserted:
            # log success
            LogSomething.log(f"[ INFO ] {url} - was stored into {col_metadata}")
        else:
            # log failed to save
            LogSomething.log(f"Something went wrong when saving URL: {url} - to {col_metadata}")

    # can remove browser
    print("[ INFO ] Deleting browser object.")
    del browser
    gc.collect()

    # PART 2: data munging
    print("[ INFO ] Constructing dataset!")
    # construct
    dataset = {
        "url": url,
        "cookies": CookieJar.from_data(metadata['data']['browser']['cookies']).data,
        "whois": WhoIsYou.from_data(metadata['data']['whois']).data,
        "network": Networked.from_data(metadata['data']['network']).data,
        "url-features": URLFeatures.from_data(metadata['data']['url-parsed'], metadata['validations']).data,
        "html": html.extract_from_data(),
        "javascript": js.extract_from_data(),
    }

    # add status target
    if target:
        dataset["target"] = target

    # if debugging and we want to see the output
    if debug:
        f = "debug_data_output"+metadata["data"]["url-parsed"]["hostname"]+"MUNGED"+".json"
        with open("./malurls/tests/files_tests/"+f, "w") as out:
            out.write(json.dumps(dataset, indent=4, sort_keys=True, default=str))
            print("[ DEBUG MODE ] Successful data MUNGING! DID NOT SAVE TO DATABASE")
            print(f"[ DEBUG MODE ] Wrote dataset out to file: {f}")

    # insert into database
    else:
        inserted = munged_url_database.insert_data(dataset)
        if inserted:
            # log success
            print(f"[ INFO ] {url} was a SUCCESS!")
            LogSomething.log(f"[ SUCCESS ] {url} - was stored into {col_dataset}")
        else:
            # log failed to save
            LogSomething.log(f"[ ERROR ] Something went wrong when saving URL: {url} - to {col_dataset}")
    # Nice.
    return


if __name__ == "__main__":
    # keep time
    start = time.time()

    # RabbitMQ message queue
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()

    channel.queue_declare(queue='urls')

    channel.basic_consume(
        receive,
        queue='urls',
        no_ack=True
    )
    print('[ * ] Waiting for messages... To exit press CTRL+C')
    # start consuming
    channel.start_consuming()
