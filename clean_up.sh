#!/usr/bin/env bash
for pid in $(ps -ef | grep 'chrome' | awk '{print $2}'); do kill -9 $pid; done