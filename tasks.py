#!/usr/bin/env python
import pika
import sys
from malurls.lib.logger import LogSomething


def main():
    meta = """
    There are 3 arguments required!
    - filename... the name of the file
    - status... Url's disposition
    - limit... how many lines to use in the file
    """
    # check
    if len(sys.argv) < 4:
        print(f"Missing arguments:\n{meta}")
        sys.exit(1)

    # params
    filename = sys.argv[1]
    status = sys.argv[2]
    limit = int(sys.argv[3])

    # RabbitMQ stuff
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))  # default port
    channel = connection.channel()

    channel.queue_declare(queue='urls')

    with open(filename, 'r') as file:
        urls = ["http://" + line.lstrip(".").rstrip("\n") for line in file if len(line) > 5]
        if limit:
            urls = urls[:limit]

    for url in urls:
        channel.basic_publish(
                exchange='',
                routing_key='urls',
                body=url
        )
        print(f"[ QUEUE INFO ] Sent {url} to queue...")

    connection.close()

    return


if __name__ == "__main__":
    msg = "[ MESSAGE QUEUE STARTED ]"
    LogSomething.log(msg)
    print(msg)
    main()
