#!/bin/bash

port=$1
shopt -s expand_aliases
alias chrome="/Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome"
chrome --headless --no-sandbox --remote-debugging-port=$port --crash-dumps-dir=~/chromelogs

